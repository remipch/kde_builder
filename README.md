# kde_builder

`kde_builder` encapsulate `kdesrc-build`, the tool to build KDE applications, in a docker.

## How to use

__1. Install the only two dependencies__

It's the only things that depend on your linux distribution

- install git
- install docker

__2. Build and enter the docker__

```bash
# Clone this repo locally
git clone https://invent.kde.org/remipch/kde_builder.git

# Clone kdesrc-build (it cannot be added as a submodule)
cd kde_builder
git clone https://invent.kde.org/sdk/kdesrc-build.git

# Choose the environment : kf6-qt6 or kf5-qt5
cd kf6-qt6

# Build the docker image (very long but done once)
./docker_build_image

# Launch the service container
./docker_up

# Open a bash session in the running container
./docker_bash
```

__3. Build and install your KDE application__

From the running container, use `kdesrc-build` to build your application.

```bash
kdesrc-build --include-dependencies kate

source /kde/build/kde/applications/kate/prefix.sh
```

__4. Run/debug your application__

From the running container, run your application, isolated from your main linux system.

```bash
# Run it
kate

# Debug it
gdb kate

# Use a full-featured IDE to debug your app
qtcreator
```

## Motivations

KDE community provides `kdesrc-build` tool to make development and contribution to KDE applications easier.

By making typical KDE developer actions simple, `kdesrc-build` is a useful and wonderful tool.

However, some limitations make contribution to KDE development difficult :

__Missing dependencies__ : KDE applications have a lot of dependencies that are not covered by `kdesrc-build`.

__Installation difficulties__ : it's difficult to install a KDE development environment for the first time. New contributors have :
- a lot of online documentation to read/understand/remember (spread over several web-pages, not always up-to-date)
- a lot of manual steps to run
- a lot of errors that must be solved manually
- a lot of guess-and-try-and-fail about things that they are not yet familiar with.

__No isolation__ : new contributors are suggested to install the required tools and frameworks in their main linux distribution.
- Of course KDE cannot support all existing linux distributions, some users may have chosen other distributions for their main linux system, in that case they have to guess what they have to do to install KDE development environment.
- A KDE contributor should be allowed to work on several open source projects even if these projects have different or incompatible requirements.
- Extra manual steps must be carefully executed to ensure that the developped/debugged KDE applications are run in isolation from the main system.

## Main goals

- Installing KDE development environment should be allowed whatever the system linux distribution is.

- Installing KDE development environment should be allowed whatever the other projects requirements are.

- Installing KDE development environment should be easy (only a few known steps)

- Installing KDE development environment should be reproducible

## Proposed approach

This project can be viewed as the first step toward the main goals listed in the previous chapter.

A few bash scripts allow to manage docker images and container :

- `docker_build_image` uses Dockerfile to build a docker image :
    - based on a basic linux image (debian12 for kf5-qt5, kdeneon for kf6-qt6)
    - installing all the dependencies

- `docker_up` uses docker-compose.yml to run a service :
    - in a continuous container (running forever)
    - using the previously built image
    - defining some useful volumes for the only files that must be exchanged between the container and the host

- `docker_bash` enters the running container and start a bash session. `docker_bash` can be called multiple times to start many bash sessions on the same running container.

## Limitations of this approach

__Limited KDE applications coverage__

The docker files in this repo meet my personal needs and only cover the KDE applications I plan to contribute to. They must be updated for other KDE applications (ideally all of them).

A solution consist of adding all dependencies of all KDE applications.

__Out-of-sync with official KDE repositories__

The official KDE repositories can add or change dependencies without notice, making this repo obsolete.

A potential solution could be to integrate this approach into official KDE repos : add a gitlab pipeline to test and guaranty that the docker files are always up-to-date.

__Increasing abstraction__

Hiding the complexity of KDE environment installation has a benefit : you don't need to solve installation problems any more.

However this abstraction has a cost : you now have to understand the added layer.

It's a win if the added layer does the job and is simple to use.

In my case I find it simpler to use a handful of small scripts.

Using this approach needs to acquire a basic mental model of the main docker concepts (image, container, volume).

__Lack of docker expertise__

I'm far from a docker expert, this repo has probably a lot of docker-newbie-errors, use it at your own risks.

## References

I didn't invent anything with this project, it's heavily inspired from my previous job. A new developer was able to quickly install the full development environment, without friction, without trouble. We were using full-featured IDEs (Clion, IntelliJ, PyCharm) from the docker container.

It's also heavily inspired from something already done in KDE : https://invent.kde.org/pim/kdepim-docker

It may be a good idea to extend this idea to all KDE repos.

The following docs have been used to make this repo :
- [KDE: Set up a development environment](https://community.kde.org/Get_Involved/development/Set_up_a_development_environment)
- [KDE: Build software with kdesrc-build](https://community.kde.org/Get_Involved/development/Build_software_with_kdesrc-build)
- [Kdevelop: Build it](https://kdevelop.org/build-it/)
- [KDE: Development/More](https://community.kde.org/Get_Involved/development/More#kf5-qt5_vs._kf6-qt6)
- [How to compile v5](https://community.kde.org/KDevelop/HowToCompile_v5)
- [Building KDevelop 5](https://kfunk.org/2016/02/16/building-kdevelop-5-from-source-on-ubuntu-15-10/)
